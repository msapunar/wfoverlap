.. _example:

#####################
Wave function overlap
#####################

.. sidebar:: Software Technical Information

  Language
    Fortran 2008

  Licence
    MIT license (MIT)

  Documentation Tool
    Doxygen.

.. contents:: :local:


.. Add technical info as a sidebar and allow text below to wrap around it

Purpose of Module
_________________
The module provides a program for calculating the overlap between two sets of CIS type wave functions.


Background Information
______________________
The module was originally developed to calculate wave function overlaps needed for numerical
nonadiabatic couplings in surface-hopping dynamics calculations. Input needed for the program is two
sets of wave functions from an electronic structure calculation, and the overlap matrix between
their basis sets.


Testing
_______



Source Code
___________

The source code is available at: Git_


.. _Git: https://bitbucket.org/msapunar/wfoverlap

