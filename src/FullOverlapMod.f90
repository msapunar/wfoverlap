!--------------------------------------------------------------------------------------------------
! MODULE: WFOverlapMod
!
! DESCRIPTION: 
!--------------------------------------------------------------------------------------------------
module fulloverlapmod
    ! Import variables
    use constants
    ! Import classes
    ! Import subroutines
    use mathmod
    implicit none
    
    private
    public :: fulloverlap

contains


    subroutine fulloverlap(rhf, n1, n2, csc, wf1, wf2, omat)
        use ciswftype
        integer, intent(in) :: rhf !< Restricted/unrestricted calculation.
        integer, intent(in) :: n1 !< Number of bra basis functions.
        integer, intent(in) :: n2 !< Number of ket basis functions.
        real(dp), intent(in) :: csc(n1, n2, rhf) ! CSC matrix for alpha/beta orbitals.
        type(ciswf), intent(in) :: wf1 !< Bra wave functions.
        type(ciswf), intent(in) :: wf2 !< Ket wave functions.
        real(dp), intent(out) :: omat(wf1%n, wf2%n) !< Overlap matrix.

        integer :: ns1 !< Number of bra wave functions.
        integer :: ns2 !< Number of ket wave functions.
        integer :: noa !< Number of occupied alpha orbitals.
        integer :: nob !< Number of occupied beta orbitals.
        integer :: nva1 !< Number of virtual alpha orbitals for bra functions.
        integer :: nva2 !< Number of virtual alpha orbitals for ket functions.
        integer :: nvb1 !< Number of virtual beta orbitals for bra functions.
        integer :: nvb2 !< Number of virtual beta orbitals for ket functions.
        real(dp), allocatable :: wfa1(:, :, :) !< Alpha coefficients of bra wfs.
        real(dp), allocatable :: wfa2(:, :, :) !< Alpha coefficients of ket wfs.
        real(dp), allocatable :: wfb1(:, :, :) !< Beta coefficients for bra wfs.
        real(dp), allocatable :: wfb2(:, :, :) !< Beta coefficients for ket wfs.

        real(dp), allocatable :: ref(:, :)
        real(dp), allocatable :: ssa(:, :)
        real(dp), allocatable :: rsa(:)
        real(dp), allocatable :: sra(:)
        real(dp) :: rra
        real(dp), allocatable :: ssb(:, :)
        real(dp), allocatable :: rsb(:)
        real(dp), allocatable :: srb(:)
        real(dp) :: rrb
        integer :: st1
        integer :: st2
        integer :: i

        ns1 = wf1%n
        ns2 = wf2%n
        noa = wf1%no(1)
        nva1 = wf1%nv(1)
        nva2 = wf2%nv(1)
        allocate(wfa1(noa, nva1, ns1))
        allocate(wfa2(noa, nva2, ns2))
        do i = 1, ns1
            wfa1(:, :, i) = wf1%c(1, i)%c / sqrt(real(rhf - wf1%rhf + 1, dp))
        end do
        do i = 1, ns2
            wfa2(:, :, i) = wf2%c(1, i)%c / sqrt(real(rhf - wf2%rhf + 1, dp))
        end do
        if (rhf == 2) then
            nob = wf1%no(wf1%rhf)
            nvb1 = wf1%nv(wf1%rhf)
            nvb2 = wf2%nv(wf2%rhf)
            allocate(wfb1(nob, nvb1, ns1))
            allocate(wfb2(nob, nvb2, ns2))
            do i = 1, ns2
                wfb1(:, :, i) = wf1%c(wf1%rhf, i)%c / sqrt(real(rhf - wf1%rhf + 1, dp))
            end do
            do i = 1, ns2
                wfb2(:, :, i) = wf2%c(wf2%rhf, i)%c / sqrt(real(rhf - wf2%rhf + 1, dp))
            end do
        end if

        allocate(ref(noa, noa))
        allocate(rsa(ns2))
        allocate(sra(ns1))
        allocate(ssa(ns1, ns2))
        ref = csc(1:noa, 1:noa, 1)
        rra = det(ref)
        call rsblock(n1, n2, csc(:, :, 1), noa, nva2, ns2, wfa2, rsa)
        call srblock(n1, n2, csc(:, :, 1), noa, nva1, ns1, wfa1, sra)
        call ssblock(n1, n2, csc(:, :, 1), noa, nva1, nva2, ns1, ns2, wfa1, wfa2, ssa)
        deallocate(ref)

        if (rhf == 1) then
            do st1 = 1, ns1
                do st2 = 1, ns2
                    omat(st1, st2) = rra * ssa(st1, st2) + sra(st1) * rsa(st2)
                end do
            end do
        else
            allocate(ref(nob, nob))
            allocate(rsb(ns2))
            allocate(srb(ns1))
            allocate(ssb(ns1, ns2))
            ref = csc(1:nob, 1:nob, 2)
            rrb = det(ref)
            call rsblock(n1, n2, csc(:, :, 2), nob, nvb2, ns2, wfb2, rsb)
            call srblock(n1, n2, csc(:, :, 2), nob, nvb1, ns1, wfb1, srb)
            call ssblock(n1, n2, csc(:, :, 2), nob, nvb1, nvb2, ns1, ns2, wfb1, wfb2, ssb)
            deallocate(ref)
            do st1 = 1, ns1
                do st2 = 1, ns2
                    omat(st1, st2) = rrb * ssa(st1, st2) + sra(st1) * rsb(st2) &
                                   + rra * ssb(st1, st2) + srb(st1) * rsb(st2)
                end do
            end do
        end if
    end subroutine fulloverlap


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SRBlock
    !
    ! DESCRIPTION:
    !----------------------------------------------------------------------------------------------
    subroutine srblock(n1, n2, csc, no, nv1, ns1, wf1, sr)
        integer, intent(in) :: n1
        integer, intent(in) :: n2
        real(dp), intent(in) :: csc(n1,n2)
        integer, intent(in) :: no
        integer, intent(in) :: nv1
        integer, intent(in) :: ns1
        real(dp), intent(in) :: wf1(no, nv1, ns1)
        real(dp), intent(out) :: sr(ns1)

        integer :: o
        integer :: v
        integer :: st
        real(dp) :: cdet
        real(dp) :: ref(1:no, 1:no)
        real(dp) :: tmp(1:no, 1:no)

        ref = csc(1:no, 1:no)
        sr = 0.0_dp
        do o = 1, no
            tmp = ref
            do v = no + 1, no + nv1
                tmp(o, :) = csc(v, 1:no)
                cdet = det(tmp)
                do st = 1, ns1
                    sr(st) = sr(st) + cdet * wf1(o, v - no, st)
                end do
            end do
        end do
    end subroutine srblock


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: RSBlock
    !
    ! DESCRIPTION:
    !----------------------------------------------------------------------------------------------
    subroutine rsblock(n1, n2, csc, no, nv2, ns2, wf2, rs)
        integer, intent(in) :: n1
        integer, intent(in) :: n2
        real(dp), intent(in) :: csc(n1,n2)
        integer, intent(in) :: no
        integer, intent(in) :: nv2
        integer, intent(in) :: ns2
        real(dp), intent(in) :: wf2(no, nv2, ns2)
        real(dp), intent(out) :: rs(ns2)

        integer :: o
        integer :: v
        integer :: st
        real(dp) :: cdet
        real(dp) :: ref(1:no, 1:no)
        real(dp) :: tmp(1:no, 1:no)

        ref = csc(1:no, 1:no)
        rs = 0.0_dp
        do o = 1, no
            tmp = ref
            do v = no + 1, no + nv2
                tmp(:, o) = csc(1:no, v)
                cdet = det(tmp)
                do st = 1, ns2
                    rs(st) = rs(st) + cdet * wf2(o, v - no, st)
                end do
            end do
        end do
    end subroutine rsblock


    
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SSBlock
    !
    ! DESCRIPTION:
    !----------------------------------------------------------------------------------------------
    subroutine ssblock(n1, n2, csc, no, nv1, nv2, ns1, ns2, wf1, wf2, ss)
        integer, intent(in) :: n1
        integer, intent(in) :: n2
        real(dp), intent(in) :: csc(n1,n2)
        integer, intent(in) :: no
        integer, intent(in) :: nv1
        integer, intent(in) :: nv2
        integer, intent(in) :: ns1
        integer, intent(in) :: ns2
        real(dp), intent(in) :: wf1(no, nv1, ns1)
        real(dp), intent(in) :: wf2(no, nv2, ns2)
        real(dp), intent(out) :: ss(ns1, ns2)

        integer :: o1
        integer :: o2
        integer :: v1
        integer :: v2
        integer :: i
        integer :: j
        integer :: st1
        integer :: st2
        real(dp) :: cdet
        real(dp) :: ref(1:no, 1:no)
        real(dp) :: tmp(1:no, 1:no)
        real(dp) :: tmp2(1:no, 1:no)

        ss = 0.0_dp
        ref = csc(1:no, 1:no)
        do o1 = 1, no
          tmp = ref
          do v1 = no + 1, no + nv1
            tmp(o1,:) = csc(v1, 1:no)
            do o2 = 1, no
              tmp2 = tmp
              do v2 = no + 1, no + nv2
                tmp2(:, o2) = csc(1:no, v2)
                tmp2(o1, o2) = csc(v1, v2)
                cdet = det(tmp2)
                do st1 = 1, ns1
                  do st2 = 1, ns2
                    ss(st1, st2) = ss(st1, st2) + cdet * wf1(o1, v1 - no, st1) * wf2(o2, v2 - no, st2)
                  end do
                end do
              end do
            end do
          end do
        end do
    end subroutine ssblock


end module fulloverlapmod
