program wfoverlap

    use constants
    use assignprobmod

    use fulloverlapmod
    use ciswftype
    implicit none
    
    real(dp), allocatable :: omat(:,:)
    real(dp), allocatable :: tmpmat(:,:)
    integer, allocatable :: row2col(:)

    integer :: rhf
    integer :: n1
    integer :: n2
    real(dp), allocatable :: csc(:, :, :)
    type(ciswf) :: wf1
    type(ciswf) :: wf2

    integer :: i
    integer :: st
    integer :: inunit

    open(newunit=inunit, file='cscmat')
    read(inunit, *) rhf, n1, n2 ! Restricted/unrestricted, # orbitals wf1, # orbitals wf2.
    allocate(csc(n1, n2, rhf))
    do st = 1, rhf
        do i = 1, n2
            read(inunit, *) csc(:, i, st)
        end do
    end do
    close(inunit)

    call ciswfread('wf1', wf1)
    call ciswfread('wf2', wf2)

    do i = 1, wf1%rhf
        if (wf1%no(i) + wf1%nv(i) /= n1) then
            write(stderr, '(x,a)') 'Error reading input.'
            write(stderr, '(3x,a,i0)') 'CSC matrix number of orbitals: ', n1
            write(stderr, '(3x,a,i0,a,i0)') 'WF 1 number of spin ', i, ' orbitals: ', &
                                            wf1%no(i) + wf1%nv(i)
            stop
        end if
    end do
    do i = 1, wf2%rhf
        if (wf2%no(i) + wf2%nv(i) /= n2) then
            write(stderr, '(x,a)') 'Error reading input.'
            write(stderr, '(3x,a,i0)') 'CSC matrix number of orbitals: ', n2
            write(stderr, '(3x,a,i0,a,i0)') 'WF 2 number of spin ', i, ' orbitals: ', &
                                            wf2%no(i) + wf2%nv(i)
            stop
        end if
    end do

    allocate(omat(wf1%n, wf2%n))
    call fulloverlap(rhf, n1, n2, csc, wf1, wf2, omat)

    allocate(tmpmat(wf1%n, wf2%n))
    allocate(row2col(wf1%n)) !< @todo wf1%n /= wf2%n option.
    tmpmat = -abs(omat)
    call assignprob(wf1%n, tmpmat, row2col)
    do st = 1, wf1%n
!       write(*,*) st, row2col(st), omat(st, row2col(st))
        if (omat(st, row2col(st)) < 0) omat(st, :) = -omat(st, :)
    end do
    do st = 1, wf1%n
        write(*,'(10000(e22.15,2x))') omat(st, :)
    end do

end program wfoverlap
