!--------------------------------------------------------------------------------------------------
! MODULE: WFOverlapMod
!
! DESCRIPTION: 
!--------------------------------------------------------------------------------------------------
module fulloverlapmod
    ! Import variables
    use constants
    ! Import classes
    ! Import subroutines
    use mathmod
    implicit none
    
    private
    public :: fulloverlap

contains


    subroutine fulloverlap(rhf, ns1, ns2, n1, n2, &
                       csca, noa, nva1, nva2, wfa1, wfa2, &
                       cscb, nob, nvb1, nvb2, wfb1, wfb2, omat)
        integer, intent(in) :: rhf !< Restricted/unrestricted calculation.
        integer, intent(in) :: ns1 !< Number of bra wave functions.
        integer, intent(in) :: ns2 !< Number of ket wave functions.
        integer, intent(in) :: n1 !< Number of bra basis functions.
        integer, intent(in) :: n2 !< Number of ket basis functions.
        real(dp), intent(in) :: csca(n1, n2) !< Overlap matrix for alpha functions.
        integer, intent(in) :: noa !< Number of occupied alpha orbitals.
        integer, intent(in) :: nva1 !< Number of virtual alpha orbitals for bra functions.
        integer, intent(in) :: nva2 !< Number of virtual alpha orbitals for ket functions.
        real(dp), intent(in) :: wfa1(noa, nva1, ns1) !< Alpha coefficients of bra wfs.
        real(dp), intent(in) :: wfa2(noa, nva2, ns2) !< Alpha coefficients of ket wfs.
        real(dp), intent(in) :: cscb(n1, n2) !< Overlap matrix for beta functions.
        integer, intent(in) :: nob !< Number of occupied beta orbitals.
        integer, intent(in) :: nvb1 !< Number of virtual beta orbitals for bra functions.
        integer, intent(in) :: nvb2 !< Number of virtual beta orbitals for ket functions.
        real(dp), intent(in) :: wfb1(nob, nvb1, ns1) !< Beta coefficients for bra wfs.
        real(dp), intent(in) :: wfb2(nob, nvb2, ns2) !< Beta coefficients for ket wfs.
        real(dp), intent(out) :: omat(ns1, ns2) !< Overlap matrix.

        real(dp), allocatable :: l2minor(:, :, :, :)
        real(dp), allocatable :: l1minor(:, :)
        real(dp), allocatable :: ref(:, :)
        real(dp), allocatable :: ssa(:, :)
        real(dp), allocatable :: rsa(:)
        real(dp), allocatable :: sra(:)
        real(dp) :: rra
        real(dp), allocatable :: ssb(:, :)
        real(dp), allocatable :: rsb(:)
        real(dp), allocatable :: srb(:)
        real(dp) :: rrb
        integer :: st1
        integer :: st2


        allocate(l2minor(noa, noa, noa, noa))
        allocate(l1minor(noa, noa))
        allocate(ref(noa, noa))
        allocate(rsa(ns2))
        allocate(sra(ns1))
        allocate(ssa(ns1, ns2))
        ref = csca(1:noa, 1:noa)
        rra = det(ref)
        call getlvl2minors(noa, ref, l2minor)
        call getlvl1minors(noa, ref, l1minor)
        call rsblock(n1, n2, csca, noa, nva2, ns2, wfa2, l1minor, rsa)
        call srblock(n1, n2, csca, noa, nva1, ns1, wfa1, l1minor, sra)
        call ssblock(n1, n2, csca, noa, nva1, nva2, ns1, ns2, wfa1, wfa2, l1minor, l2minor, ssa)
        deallocate(l2minor)
        deallocate(l1minor)
        deallocate(ref)

        if (rhf == 1) then
            do st1 = 1, ns1
                do st2 = 1, ns2
                    omat(st1, st2) = rra * ssa(st1, st2) + sra(st1) * rsa(st2)
                end do
            end do
        else
            allocate(l2minor(nob, nob, nob, nob))
            allocate(l1minor(nob, nob))
            allocate(ref(nob, nob))
            allocate(rsb(ns2))
            allocate(srb(ns1))
            allocate(ssb(ns1, ns2))
            ref = cscb(1:nob, 1:nob)
            rrb = det(ref)
            call getlvl2minors(nob, ref, l2minor)
            call getlvl1minors(nob, ref, l1minor)
            call rsblock(n1, n2, cscb, nob, nvb2, ns2, wfb2, l1minor, rsb)
            call srblock(n1, n2, cscb, nob, nvb1, ns1, wfb1, l1minor, srb)
            call ssblock(n1, n2, cscb, nob, nvb1, nvb2, ns1, ns2, wfb1, wfb2, l1minor, l2minor, ssb)
            deallocate(l2minor)
            deallocate(l1minor)
            deallocate(ref)
            do st1 = 1, ns1
                do st2 = 1, ns2
                    omat(st1, st2) = rrb * ssa(st1, st2) + sra(st1) * rsb(st2) &
                                   + rra * ssb(st1, st2) + srb(st1) * rsb(st2)
                end do
            end do
        end if
    end subroutine fulloverlap


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SRBlock
    !
    ! DESCRIPTION:
    !----------------------------------------------------------------------------------------------
    subroutine srblock(n1, n2, csc, no, nv1, ns1, wf1, l1minor, sr)
        integer, intent(in) :: n1
        integer, intent(in) :: n2
        real(dp), intent(in) :: csc(n1,n2)
        integer, intent(in) :: no
        integer, intent(in) :: nv1
        integer, intent(in) :: ns1
        real(dp), intent(in) :: wf1(no, nv1, ns1)
        real(dp), intent(in) :: l1minor(no, no)
        real(dp), intent(out) :: sr(ns1)

        integer :: o
        integer :: v
        integer :: i
        integer :: st
        real(dp) :: msum

        sr = 0.0_dp
        do o = 1, no
            do v = no + 1, no + nv1
                msum = 0
                do i = 1, no
                    msum = msum + (-1)**(o + i) * csc(v, i) * l1minor(o, i)
                end do
                do st = 1, ns1
                    sr(st) = sr(st) + msum * wf1(o, v - no, st)
                end do
            end do
        end do
    end subroutine srblock


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: RSBlock
    !
    ! DESCRIPTION:
    !----------------------------------------------------------------------------------------------
    subroutine rsblock(n1, n2, csc, no, nv2, ns2, wf2, l1minor, rs)
        integer, intent(in) :: n1
        integer, intent(in) :: n2
        real(dp), intent(in) :: csc(n1,n2)
        integer, intent(in) :: no
        integer, intent(in) :: nv2
        integer, intent(in) :: ns2
        real(dp), intent(in) :: wf2(no, nv2, ns2)
        real(dp), intent(in) :: l1minor(no, no)
        real(dp), intent(out) :: rs(ns2)

        integer :: o
        integer :: v
        integer :: i
        integer :: st
        real(dp) :: msum

        rs = 0.0_dp
        do o = 1, no
            do v = no + 1, no + nv2
                msum = 0
                do i = 1, no
                    msum = msum + (-1)**(o + i) * csc(i, v) * l1minor(i, o)
                end do
                do st = 1, ns2
                    rs(st) = rs(st) + msum * wf2(o, v - no, st)
                end do
            end do
        end do
    end subroutine rsblock


    
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SSBlock
    !
    ! DESCRIPTION:
    !----------------------------------------------------------------------------------------------
    subroutine ssblock(n1, n2, csc, no, nv1, nv2, ns1, ns2, wf1, wf2, l1minor, l2minor, ss)
        integer, intent(in) :: n1
        integer, intent(in) :: n2
        real(dp), intent(in) :: csc(n1,n2)
        integer, intent(in) :: no
        integer, intent(in) :: nv1
        integer, intent(in) :: nv2
        integer, intent(in) :: ns1
        integer, intent(in) :: ns2
        real(dp), intent(in) :: wf1(no, nv1, ns1)
        real(dp), intent(in) :: wf2(no, nv2, ns2)
        real(dp), intent(in) :: l2minor(no, no, no, no)
        real(dp), intent(in) :: l1minor(no, no)
        real(dp), intent(out) :: ss(ns1, ns2)

        integer :: o1
        integer :: o2
        integer :: v1
        integer :: v2
        integer :: i
        integer :: j
        integer :: st1
        integer :: st2
        real(dp) :: msum
        integer :: sgnj
        integer :: sgn
        integer :: s(4)

        ss = 0.0_dp
        do o1 = 1, no
          do o2 = 1, no
            do v1 = no + 1, no + nv1
              do v2 = no + 1, no + nv2
                msum = 0
                s(1) = o1
                do i = 1, no
                  if (i > o1) then
                      s(1) = i
                  else if (i < o1) then
                      s(2) = i
                  else
                      s(2) = o1
                      cycle
                  end if
                  sgnj = 1
                  s(3) = o2
                  do j = 1, no
                      if (j > o2) then
                          s(3) = j
                      else if (j < o2) then
                          s(4) = j
                      else
                          sgnj = -1
                          s(4) = o2
                          cycle
                      end if
                      sgn = sgnj * (-1)**(o1+o2+i+j)
                      msum = msum + sgn * csc(v1, j) * csc(i, v2) * l2minor(s(1), s(2), s(3), s(4))
                  end do
                end do
                msum = msum + (-1)**(o1+o2) * l1minor(o1, o2) * csc(v1, v2)
                do st1 = 1, ns1
                  do st2 = 1, ns2
                    ss(st1, st2) = ss(st1, st2) + msum * wf1(o1, v1 - no, st1) * wf2(o2, v2 - no, st2)
                  end do
                end do
              end do
            end do
          end do
        end do
    end subroutine ssblock


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: GetLvl1Minors
    !
    ! DESCRIPTION:
    !> @brief 
    !----------------------------------------------------------------------------------------------
    subroutine getlvl1minors(n, mat, dets)
        integer, intent(in) :: n
        real(dp), intent(in) :: mat(n,n)
        real(dp), intent(out) :: dets(n,n)
        
        integer :: r
        integer :: c
        integer :: seq(n)
        integer :: cmask(n-1)
        integer :: rmask(n-1)
        real(dp) :: minor(n-1, n-1)

        do r = 1, n
            seq(r) = r
        end do
        do r = 1, n
            rmask = pack(seq, seq/=r)
            do c = 1, n
                cmask = pack(seq, seq/=c)
                minor = mat(rmask, cmask)
                dets(r,c) = det(minor)
            end do
        end do
    end subroutine getlvl1minors


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: GetLvl2Minors
    !
    ! DESCRIPTION:
    !> @brief 
    !----------------------------------------------------------------------------------------------
    subroutine getlvl2minors(n, mat, dets)
        integer, intent(in) :: n
        real(dp), intent(in) :: mat(n,n)
        real(dp), intent(out) :: dets(n,n,n,n)
        
        integer :: r1
        integer :: r2
        integer :: c1
        integer :: c2
        integer :: seq(n)
        integer :: cmask(n-2)
        integer :: rmask(n-2)
        real(dp) :: minor(n-2, n-2)

        do r1 = 1, n
            seq(r1) = r1
        end do
        do r1 = 1, n
            do r2 = 1, r1 - 1
                rmask = pack(seq, (seq/=r1 .and. seq/=r2))
                do c1 = 1, n
                    do c2 = 1, c1 - 1
                        cmask = pack(seq, (seq/=c1 .and. seq/=c2))
                        minor = mat(rmask, cmask)
                        dets(r1,r2,c1,c2) = det(minor)
                    end do
                end do
            end do
        end do
    end subroutine getlvl2minors


end module fulloverlapmod
